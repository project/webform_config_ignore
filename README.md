# Webform Config Ignore

Webform Config Ignore adds a filter to configuration import and export to skip webforms and webform options. This allows
site editors to change webforms and options lists without having to fear obliterating their work on a config import.

## Requirements

This module requires the Config Filter module (8.x-1.x).

## Installation

1. Install the Config Filter module:

```php
composer require drupal/config_filter:^1.0
```

2. Install and enable the Webform Config Ignore module.

## Usage

Once installed and enabled, the module will automatically add a filter to configuration import and export processes.
This filter skips webforms and webform options, preventing their configuration from being overwritten during
deployments.

A setting is available to allow imports and exports on development environments, ensuring that changes to webforms can
still be synchronized between instances.

## Configuration

To disable the filter, add the following line to your settings.php file:

```php
$settings['webform_config_ignore_disabled'] = TRUE;
```

## Maintainers

- [Heine](https://www.drupal.org/u/heine)
- [Martijn Vermeulen (Marty2081)](https://www.drupal.org/u/marty2081)

Initial development

- [LimoenGroen](https://www.drupal.org/limoengroen)

Thanks to [Sutharsan](https://www.drupal.org/u/sutharsan) for the Drupal 9 patch
and [chrisssnyder](https://www.drupal.org/u/chrissnyder) for committing it.